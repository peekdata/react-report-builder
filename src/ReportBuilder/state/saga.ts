import {
  ICompatibilityCheckRequest,
  ICompatibilityResponse,
  INode,
  INotOptimizedReportResponse,
  IOptimizedReportResponse,
  IReportRequest,
  IReportRequestSortings,
  PeekdataError,
} from "peekdata-datagateway-api-sdk";
import { all, put, select, take, takeLatest } from "redux-saga/effects";
import { ISelectedNode } from "src/ReportBuilder/models/node";
import { peekdataApi } from "src/ReportBuilder/services/api";
import { IAction } from "src/ReportBuilder/state/action";
import {
  actionTypes,
  changeLimitRowsTo,
  changeStartWithRow,
  compatibilityChecked,
  csvFileLoaded,
  dataFullLoaded,
  dataModelNamesLoaded,
  dataOptimizedLoaded,
  dimensionsLoaded,
  filtersLoaded,
  generateReportRequest,
  ILoadNodesPayloadRequest,
  loadDataModelNames,
  loadDataModelNodes,
  loadScopeNames,
  metricsLoaded,
  reportRequestGenerated,
  scopeNamesLoaded,
  selectDataModel,
  selectLoaded,
  setFilters,
  setSelectedDimensions,
  setSelectedMetrics,
} from "src/ReportBuilder/state/actions";
import { IReportBuilderState } from "src/ReportBuilder/state/reducers";
import { IScopeNamesState } from "src/ReportBuilder/state/reducers/scopeNames";
import {
  getRequestDateRangesFilters,
  getRequestOptions,
  getRequestSingleKeysFilters,
  getRequestSingleValuesFilters,
  getRequestSortedOptions,
} from "src/ReportBuilder/utils/RequestUtils";
import v4 from "uuid/v4";
import { defaultRows } from "../constants/rows";
import { FilterTypes, IFilter } from "../models/filter";
import { ITranslations } from "../models/translations";
import { isPeekdataError } from "../utils/ErrorUtils";

// #region -------------- Load report request -------------------------------------------------------------------

function* onLoadReportRequest(action: IAction<Partial<IReportRequest>>) {
  try {
    const reportRequest = action.payload;
    const scopeName = reportRequest && reportRequest.scopeName;

    if (!reportRequest || !scopeName) {
      yield put(loadScopeNames());
      return;
    }

    const { dimensions, metrics, sortings, filters, options, dataModelName } =
      reportRequest;
    const optionsRows = options && options.rows;
    const startWithRow = optionsRows && optionsRows.startWithRow;
    const limitRowsTo = optionsRows && optionsRows.limitRowsTo;

    yield put(loadScopeNames(true));
    yield take(actionTypes.scopeNamesLoaded);

    yield put(loadDataModelNames(scopeName));
    yield take(actionTypes.dataModelNamesLoaded);

    if (dataModelName) {
      const selectedDimensions = getSelectedDimensions(dimensions, sortings);
      const selectedMetrics = getSelectedMetrics(metrics, sortings);

      yield put(
        loadDataModelNodes({
          selectedDataModel: dataModelName,
          selectedDimensions,
          selectedMetrics,
        })
      );

      yield take(actionTypes.dimensionsLoaded);
      yield put(filtersLoaded(filters));

      yield put(selectDataModel(null));
      yield take(actionTypes.compatibilityChecked);

      yield put(
        changeStartWithRow(
          startWithRow !== undefined && startWithRow !== null
            ? Number(startWithRow)
            : defaultRows.offset
        )
      );
      yield put(
        changeLimitRowsTo(
          limitRowsTo !== undefined && limitRowsTo !== null
            ? Number(limitRowsTo)
            : defaultRows.limit
        )
      );
      yield put(reportRequestGenerated(reportRequest));
    }
  } catch (error) {
    console.error(error);
  }
}

const getSelectedDimensions = (
  dimensions: string[],
  sortings: IReportRequestSortings
): ISelectedNode[] =>
  dimensions &&
  dimensions.map((value) => ({
    value,
    sorting: getSorting(sortings, value, "dimensions"),
  }));

const getSelectedMetrics = (
  metrics: string[],
  sortings: IReportRequestSortings
): ISelectedNode[] =>
  metrics &&
  metrics.map((value) => ({
    value,
    sorting: getSorting(sortings, value, "metrics"),
  }));

const getSorting = (
  sortings: IReportRequestSortings,
  value: string,
  key: string
) => {
  const sorting =
    sortings && sortings[key] && sortings[key].find((d) => d.key === value);

  if (!sorting) {
    return null;
  }

  return sorting && sorting.direction ? sorting.direction : null;
};

// #endregion

// #region -------------- Generate report request -------------------------------------------------------------------

function* onGenerateReportRequest() {
  try {
    const {
      scopeNames,
      dataModelNames,
      selectedDimensions,
      selectedMetrics,
      filters,
      limitRowsTo,
      startWithRow,
    }: IReportBuilderState = yield select((state) => state);

    const scopeName = scopeNames && scopeNames.selectedScope;
    const dataModelName = dataModelNames && dataModelNames.selectedDataModel;

    const dimensions = getRequestOptions(selectedDimensions);
    const metrics = getRequestOptions(selectedMetrics);
    const sortedDimensions = getRequestSortedOptions(selectedDimensions);
    const sortedMetrics = getRequestSortedOptions(selectedMetrics);
    const dateRanges = getRequestDateRangesFilters(filters);
    const singleKeys = getRequestSingleKeysFilters(filters);
    const singleValues = getRequestSingleValuesFilters(filters);

    const request: IReportRequest = {
      scopeName,
      dataModelName,
      requestID: v4(),
      consumerInfo: "Peekdata-Report-Builder-Demo",
      metrics,
    };

    if (dimensions && dimensions.length > 0) {
      request.dimensions = dimensions;
    }

    if (dateRanges && dateRanges.length > 0) {
      request.filters = {
        ...request.filters,
        dateRanges,
      };
    }

    if (singleKeys && singleKeys.length > 0) {
      request.filters = {
        ...request.filters,
        singleKeys,
      };
    }

    if (singleValues && singleValues.length > 0) {
      request.filters = {
        ...request.filters,
        singleValues,
      };
    }

    if (sortedDimensions && sortedDimensions.length > 0) {
      request.sortings = {
        ...request.sortings,
        dimensions: sortedDimensions,
      };
    }

    if (sortedMetrics && sortedMetrics.length > 0) {
      request.sortings = {
        ...request.sortings,
        metrics: sortedMetrics,
      };
    }

    request.options = {
      rows: {
        startWithRow:
          startWithRow !== null && startWithRow !== undefined
            ? startWithRow.toString()
            : defaultRows.offset.toString(),
        limitRowsTo:
          limitRowsTo !== null && limitRowsTo !== undefined
            ? limitRowsTo.toString()
            : defaultRows.limit.toString(),
      },
    };

    yield put(reportRequestGenerated(request));
  } catch (error) {}
}

// #endregion

// #region -------------- Load scope names -------------------------------------------------------------------

function* onLoadScopeNames() {
  try {
    const scopeNames: string[] = yield peekdataApi.datamodel.getScopeNames();

    yield put(
      scopeNamesLoaded({
        data: scopeNames,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      scopeNamesLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load datamodel names -------------------------------------------------------------------

function* onLoadDataModelNames(action: IAction<string>) {
  try {
    const dataModelNames: string[] =
      yield peekdataApi.datamodel.getDataModelNames(action.payload);

    yield put(
      dataModelNamesLoaded({
        data: dataModelNames,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      dataModelNamesLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load dimensions -------------------------------------------------------------------

function* onLoadDimensions(action: IAction<ILoadNodesPayloadRequest>) {
  try {
    const scopeNames: IScopeNamesState = yield select(
      (state: IReportBuilderState) => state.scopeNames
    );
    const selectedScope = scopeNames && scopeNames.selectedScope;
    const { selectedDataModel, selectedDimensions } = action.payload;

    const request: ICompatibilityCheckRequest = {
      scopeName: selectedScope,
      dataModelName: selectedDataModel,
      metrics: [],
      dimensions: [],
    };

    const dimensions: INode[] = yield peekdataApi.datamodel
      .getCompatibleNodes(request)
      .then((response) => response.dimensions);

    yield put(
      dimensionsLoaded({
        data: dimensions,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );

    if (selectedDimensions) {
      yield put(setSelectedDimensions(selectedDimensions));
    }
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      dimensionsLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load metrics -------------------------------------------------------------------

function* onLoadMetrics(action: IAction<ILoadNodesPayloadRequest>) {
  try {
    const scopeNames: IScopeNamesState = yield select(
      (state: IReportBuilderState) => state.scopeNames
    );
    const selectedScope = scopeNames && scopeNames.selectedScope;
    const { selectedDataModel, selectedMetrics } = action.payload;

    const request: ICompatibilityCheckRequest = {
      scopeName: selectedScope,
      dataModelName: selectedDataModel,
      dimensions: [],
      metrics: [],
    };

    const metrics: INode[] = yield peekdataApi.datamodel
      .getCompatibleNodes(request)
      .then((response) => response.metrics);

    yield put(
      metricsLoaded({
        data: metrics,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );

    if (selectedMetrics) {
      yield put(setSelectedMetrics(selectedMetrics));
    }
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      metricsLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Check compatibility -------------------------------------------------------------------

function* onCheckCompatibility() {
  try {
    const {
      scopeNames,
      dataModelNames,
      selectedDimensions,
      selectedMetrics,
    }: IReportBuilderState = yield select((state) => state);

    const scopeName = scopeNames && scopeNames.selectedScope;
    const dataModelName = dataModelNames && dataModelNames.selectedDataModel;

    const dimensions = getRequestOptions(selectedDimensions);
    const metrics = getRequestOptions(selectedMetrics);

    const compatibilityRequest: ICompatibilityCheckRequest = {
      scopeName,
      dataModelName,
      requestID: v4(),
      consumerInfo: "Peekdata-Report-Builder-Demo",
      dimensions,
      metrics,
    };

    const compatibility: ICompatibilityResponse =
      yield peekdataApi.datamodel.getCompatibleNodes(compatibilityRequest);

    if (compatibility && (compatibility.message || compatibility.errorCode)) {
      throw new PeekdataError(compatibility.message, compatibility.errorCode);
    }

    yield put(
      compatibilityChecked({
        data: compatibility,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );

    const filtersState: IFilter[] = yield select(
      (state: IReportBuilderState) => state.filters
    );
    const compatibilityDimensions = compatibility.dimensions.map(
      (dimension) => dimension.name
    );
    const compatibilityFilters: IFilter[] = [];

    for (const filter of filtersState) {
      let isCompatible = false;

      if (!filter.filterType) {
        compatibilityFilters.push(filter);
      } else if (
        filter.filterType === FilterTypes.DateRanges ||
        filter.filterType === FilterTypes.SingleKeys
      ) {
        for (const dimension of compatibilityDimensions) {
          if (filter.key === dimension) {
            isCompatible = true;
            break;
          }
        }

        if (isCompatible || !filter.key) {
          compatibilityFilters.push(filter);
        }
      } else if (filter.filterType === FilterTypes.SingleValues) {
        isCompatible = true;

        for (const key of filter.keys) {
          if (compatibilityDimensions.indexOf(key) === -1) {
            isCompatible = false;
            break;
          }
        }

        if (isCompatible || !filter.keys || filter.keys.length < 1) {
          compatibilityFilters.push(filter);
        }
      }
    }

    yield put(setFilters(compatibilityFilters));
    yield put(generateReportRequest());
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      compatibilityChecked({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load data full -------------------------------------------------------------------

function* onLoadDataFull(action: IAction<IReportRequest>) {
  try {
    const dataFull: INotOptimizedReportResponse =
      yield peekdataApi.data.getData(action.payload);

    yield put(
      dataFullLoaded({
        data: dataFull,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      dataFullLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load data optimized -------------------------------------------------------------------

function* onLoadDataOptimized(action: IAction<IReportRequest>) {
  try {
    const dataOptimized: IOptimizedReportResponse =
      yield peekdataApi.data.getDataOptimized(action.payload);

    yield put(
      dataOptimizedLoaded({
        data: dataOptimized,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      dataOptimizedLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load csv file -------------------------------------------------------------------

function* onLoadCsvFile(action: IAction<IReportRequest>) {
  try {
    const file: string = yield peekdataApi.data.getCSV(action.payload);

    yield put(
      csvFileLoaded({
        data: file,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      csvFileLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Load select -------------------------------------------------------------------

function* onLoadSelect(action: IAction<IReportRequest>) {
  try {
    const selectStr: string = yield peekdataApi.data.getQuery(action.payload);

    yield put(
      selectLoaded({
        data: selectStr,
        isFetching: false,
        error: null,
        errorTimestamp: null,
      })
    );
  } catch (error) {
    const message = yield getErrorMessage(error);

    yield put(
      selectLoaded({
        data: null,
        isFetching: false,
        error: message,
        errorTimestamp: new Date(),
      })
    );
  }
}

// #endregion

// #region -------------- Helpers -------------------------------------------------------------------

function* getErrorMessage(error: Error) {
  if (isPeekdataError(error)) {
    const translations: ITranslations = yield select(
      (state: IReportBuilderState) => state.translations
    );
    const apiErrors = translations && translations.apiErrors;
    const translation = apiErrors && apiErrors[error.code];

    return translation || error.toString();
  }

  return error.toString();
}

// #endregion

export function* rootSaga() {
  yield all([
    takeLatest(actionTypes.loadReportRequest, onLoadReportRequest),
    takeLatest(actionTypes.generateReportRequest, onGenerateReportRequest),
    takeLatest(actionTypes.loadScopeNames, onLoadScopeNames),
    takeLatest(actionTypes.loadDataModelNames, onLoadDataModelNames),
    takeLatest(actionTypes.loadDataModelNodes, onLoadDimensions),
    takeLatest(actionTypes.loadDataModelNodes, onLoadMetrics),
    takeLatest(actionTypes.selectOption, onCheckCompatibility),
    takeLatest(actionTypes.unselectOption, onCheckCompatibility),
    takeLatest(actionTypes.loadDataOptimized, onLoadDataOptimized),
    takeLatest(actionTypes.loadDataFull, onLoadDataFull),
    takeLatest(actionTypes.loadCsvFile, onLoadCsvFile),
    takeLatest(actionTypes.loadSelect, onLoadSelect),
  ]);
}
