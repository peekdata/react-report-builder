import { AsyncState, IAction, IAsyncState } from 'src/ReportBuilder/state/action';
import { actionTypes } from 'src/ReportBuilder/state/actions';

// #region -------------- State -------------------------------------------------------------------

export interface IDataModelNamesState {
  dataModelNames: IAsyncState<string[]>;
  selectedDataModel: string;
}

const initialState: IDataModelNamesState = {
  dataModelNames: new AsyncState(),
  selectedDataModel: '',
};

// #endregion

// #region -------------- Reducer -------------------------------------------------------------------

export function dataModelNames(state: IDataModelNamesState = initialState, action: IAction): IDataModelNamesState {
  switch (action.type) {
    case actionTypes.loadScopeNames:
      if (action.payload) {
        return state;
      }

      return initialState;

    case actionTypes.loadDataModelNames:
      return {
        ...initialState,
        dataModelNames: {
          ...state.dataModelNames,
          isFetching: true,
          timestamp: new Date(),
        },
      };

    case actionTypes.dataModelNamesLoaded:
      return {
        ...state,
        dataModelNames: {
          ...state.dataModelNames,
          ...action.payload,
        },
      };

    case actionTypes.loadDataModelNodes:
      return {
        ...state,
        selectedDataModel: action.payload.selectedDataModel,
      };

    default:
      return state;
  }
}

// #endregion
