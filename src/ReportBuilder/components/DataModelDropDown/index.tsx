import { faSave } from "@fortawesome/free-solid-svg-icons";
import { IReportRequest } from "peekdata-datagateway-api-sdk";
import React from "react";
import { DropDownList } from "src/ReportBuilder/components/DropDownList";
import { ITranslations } from "src/ReportBuilder/models/translations";
import { IAsyncState } from "src/ReportBuilder/state/action";
import { ILoadNodesPayloadRequest } from "src/ReportBuilder/state/actions";
import { ButtonWithIcon } from "../ButtonWithIcon";

// #region -------------- Interfaces -------------------------------------------------------------------

interface IDefaultProps {
  showScopesDropdown: boolean;
  showDataModelsDropdown: boolean;
}

interface IProps extends IDefaultProps {
  scopeNames: IAsyncState<string[]>;
  dataModelNames: IAsyncState<string[]>;
  selectedScope: string;
  selectedDataModel: string;
  t: ITranslations;
  onScopeChanged: (scope: string) => void;
  onDataModelChanged: (payload: ILoadNodesPayloadRequest) => void;
  request: IReportRequest;
  onSaveReportRequest?: (request: IReportRequest) => void;
}

// #endregion

// #region -------------- Component -------------------------------------------------------------------

export class DataModelDropDown extends React.Component<IProps> {
  public static defaultProps: IDefaultProps = {
    showScopesDropdown: true,
    showDataModelsDropdown: true,
  };

  public render() {
    return (
      <div>
        <div className="row hidden-xs">
          {this.renderScopeDesktopTitle()}
          {this.renderDataModelDesktopTitle()}
        </div>
        <div className="row">
          {this.renderScopeDropDown()}
          {this.renderDataModelDropDown()}
          {this.renderSaveButton()}
        </div>
      </div>
    );
  }

  // #region -------------- Scope dropdown -------------------------------------------------------------------

  private renderScopeDesktopTitle = () => {
    const { showScopesDropdown, t } = this.props;

    if (!showScopesDropdown) {
      return null;
    }

    return (
      <div className="col-md-4 col-sm-5">
        <div className="rb-title-dark rb-title-small">
          {t.scopesDropdownTitle}
        </div>
      </div>
    );
  };

  private renderScopeDropDown = () => {
    const { scopeNames, selectedScope, onScopeChanged, showScopesDropdown, t } =
      this.props;
    if (!showScopesDropdown) {
      return null;
    }

    const scopeNamesData = scopeNames && scopeNames.data;
    const list = scopeNamesData && new Map(scopeNamesData.map((d) => [d, d]));

    return (
      <div className="col-md-4 col-sm-5">
        <div className="rb-title-dark rb-title-small visible-xs">
          {t.scopesDropdownTitle}
        </div>
        <DropDownList
          list={list}
          defaultLabel={t.scopesPlaceholder}
          selected={selectedScope}
          itemSelectedCallback={onScopeChanged}
        />
      </div>
    );
  };

  // #endregion

  // #region -------------- DataModel dropdown -------------------------------------------------------------------

  private renderDataModelDesktopTitle = () => {
    const { showDataModelsDropdown: showDataModelsDropdown, t } = this.props;

    if (!showDataModelsDropdown) {
      return null;
    }

    return (
      <div className="col-md-4 col-sm-5">
        <div className="rb-title-dark rb-title-small">
          {t.dataModelsDropdownTitle}
        </div>
      </div>
    );
  };

  private renderDataModelDropDown = () => {
    const {
      dataModelNames: dataModelNames,
      selectedDataModel: selectedDataModel,
      showDataModelsDropdown: showDataModelsDropdown,
      t,
    } = this.props;

    if (!showDataModelsDropdown) {
      return null;
    }

    const dataModelNamesData = dataModelNames && dataModelNames.data;
    const list =
      dataModelNamesData && new Map(dataModelNamesData.map((d) => [d, d]));

    return (
      <div className="col-md-4 col-sm-5">
        <div className="rb-title-dark rb-title-small visible-xs">
          {t.dataModelsDropdownTitle}
        </div>
        <DropDownList
          list={list}
          defaultLabel={t.dataModelsPlaceholder}
          selected={selectedDataModel}
          itemSelectedCallback={this.onDataModelChanged}
        />
      </div>
    );
  };

  private showSaveButton = () => {
    const { request } = this.props;

    return (
      request &&
      ((request.metrics && request.metrics.length > 0) ||
        (request.dimensions && request.dimensions.length > 0))
    );
  };

  private renderSaveButton = () => {
    const { t, onSaveReportRequest, request } = this.props;
    if (!this.showSaveButton() || !onSaveReportRequest) return;
    return (
      <div style={{ width: "100%" }}>
        <ButtonWithIcon
          icon={faSave}
          styleClasses="rb-btn-small rb-btn-crimson"
          style={{
            float: "right",
            marginRight: 13,
          }}
          onClick={() => onSaveReportRequest(request)}
          title={t.saveButtonText}
        />
      </div>
    );
  };

  private onDataModelChanged = (selectedDataModel: string) => {
    const { onDataModelChanged } = this.props;

    onDataModelChanged({ selectedDataModel });
  };

  // #endregion
}

// #endregion
