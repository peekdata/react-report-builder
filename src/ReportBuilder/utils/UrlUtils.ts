/**
 * Generates cUrl
 * @param body Body
 * @param path Relative path
 */
export function generateCUrl(body: string, path: string, baseURL: string) {
  return `curl -XPOST -H "Content-type: application/json" -d ${JSON.stringify(body)} "${baseURL}${path}"`;
}
